*2021-03-23*

Richard Stallman is being unfairly lynched and defamed by a group of people in an open letter to the FSF. They are asking for him to be expelled from an organization he created, he commited his live to, and he deserves more than any one of us to be head of. 

We, the undersigned, belive in freedom as a human right. Freedom of speach is what Richard Stallman is being attacked for. In order to realize the promise of everything freedom makes possible, we must defend free speech and we must defend our community members against these kind of attacks to free speech based on false acusations and regrettable hate campaigns. Such hate campaigns as we know only lead to division and self destruction.

We acknowledge that some of Richard Stallman's opinions are controversial. Some of us might have different opinions, but we are mature enough to differentiate an opinion from bad behaviour. Commenting about a controversial topic is not a crime and should never be a crime. Commenting about a controversial topic should have never been a reason for Richard Stallman (or any one else) to lose his job, his reputation, his position and access to the projects he has been devoted to most of his life.

We accept and recognize Richard as a human being with strengths and weaknesses as any one else. We are thankful for all the work he has done in the past years in favor of software freedom. We acknowledge the need for more people like Richard in the head of key organizations like the Free Software Foundation and the GNU project.

There has been enough damage against Richard Stallman, the Free Software Foundation and the Free software movement as a whole. We have had enough. We have had enough *ad-hóminem* attacks targeted against Free Software representatives. We have had enough divisive attacks aiming to leave our Free Software movement weaker than ever before.

**We are calling for the endorsement of Richard M. Stallman as a Board member of the Free Software Foundation.** FSF members should recognize Richard for his commitment to software freedom and for his great job all these years. They should embrace freedom as a whole, despite of malicious dafamation attacks comming from people with personal interests against the person and the movement. It is time for RMS to recover his lost position and never step back again. Free Software movement needs more people with such a commitment for Free Software's ideals. 

**We are also calling for Richard M. Stallman to be restored to his original position as FSF's president and keep his current position in the GNU Project.** 

We urge those in a position to do so to keep supporting Richard Stallman and his return to the Free Software Foundation. Keep contributing to projects related to Free Software and RMS. Keep attending to Free Software events, and events that welcome RMS and other Free Software representatives. We ask for contributors to Free Software projects to take a stand against bigotry and hate within their projects. While doing these things, tell these communities the value of Freedom. 

We acknowledge all of Richard Stallman's contributions to software freedom. Some of us have our own stories about RMS and our interactions with him. We have welcomed him in our homes and our universities. We have learned to know him as the human he is, with his strengths and weaknesses, things that are not captured in email threads or on video. We hope you will read what has been shared and consider the harm some malicious people are doing to our community.

----

Richard Stallman está siendo linchado y difamado por un grupo de personas en una carta abierta a la FSF. Están pidiendo que sea expulsado de una organización que él creó, que dedicó su vida a ella y que merece más que cualquiera de nosotros estar a la cabeza.

Nosotros, los aquí firmantes, creemos en la libertad como un derecho humano. Libertad de expresión es por lo que Richard Stallman ha estado siendo atacado. Para poder alcanzar la promesa de todo lo que la libertad hace posible, debemos defender la libertad de expresión y debemos defender a los miembros de nuestra comunidad contra este tipo de ataques contra la libre expresión basados en falsas acusaciones y lamentables campañas de odio. Estas campañas de odio, como sabemos, solo nos llevan a la división y a la autodestrucción.

Reconocemos que algunas de las opiniones de Richard Stallman son controvertidas. Algunos de nosotros podríamos tener distintas opiniones, pero ya estamos lo suficientemente maduros para poder distinguir entre una opinión y un mal comportamiento. Opinar sobre temas polémicos no es un crimen y nunca debería serlo. Haber opinado sobre un tema polémico nunca debió haber sido razón para que Richard Stallman (o cualquier otro) perdiera su empleo, su reputación, su posición y acceso a proyectos a los que ha dedicado parte de su vida.

Aceptamos y reconocemos a Richard Stallman como un ser humano con virtudes y defectos como cualquier otro. Estamos agradecidos por todo el trabajo que ha hecho en estos años en favor de la libertad del software. Reconocemos la necesidad de que más personas como Richard estén a la cabeza de organizaciones clave como al Free Software Foundation y el proyecto GNU.

Ya ha habido suficiente daño en contra de Richard Stallman, la free Software Foundation y el movimiento de Software Libre como un todo. Ya tuvimos suficiente. Ya hemos tenido suficientes ataques *ad-hóminem* en contra de representantes del Software Libre. Ya hemos tenido suficientes ataques divisivos que solo buscan dejar a nuestro movimiento del Software Libre más débil que nunca antes.

**Estamos haciendo un llamado a respaldar a Richard M. Stallman como miembro de la Junta de la Free Software foundation.** Los miembros de la FSF deberían reconocer a Richard por su compromiso hacia la libertad del software y por su gran trabajo todos estos años. Deberían abrazar la libertad como un todo, a pesar de ataques malintencionados provenientes de personas con intereses personales en contra de la persona y del movimiento. Es hora de que RMS recupere su posición perdida y nunca vuelva a retroceder. El movimiento del Software Libre necesita más personas con ese compromiso hacia los ideales del Software Libre.

**Estamos haciendo un llamado para que Richard M. Stallman sea devuelto a su posición original como presidente de la FSF y mantenga su posición actual en el proyecto GNU.**

Instamos a quienes estén en posición de hacerlo, a que sigan apoyando a Richard Stallman y su regreso a la Free Software Foundation. Que sigan contribuyendo en proyectos relacionados con el Software Libre y RMS. Que sigan asistiendo a eventos de Software Libre, a eventos que reciban a RMS y otros representantes del Software Libre. Pedimos a quienes contribuyen en proyectos de Software Libre que tomen una posición en contra de la intolerancia y el odio hacia dentro de sus proyectos.  Y mientras lo hagan, que hablen a estas comunidades sobre el valor de la libertad.

Reconocemos todas las contribuciones de Richard Stallman a la libertad del software. Algunos de nosotros tenemos nuestras propias historias sobre Richard y cómo hemos convivido con él. lo hemos recibido en nuestras casas y en nuestras universidades. hemos aprendido a conocerlo como el ser humano que es, con sus virtudes y sus defectos, cosas que no pueden ser capturadas en listas de correo o en video. Esperamos que lean cuidadosamente lo que ha sido compartido y que consideren el daño que gente malintencionada está haciendo a nuestra comunidad.

----

To sign, please email <rms-support@softwarelibre.mx> or [submit a merge request](https://gitlab.com/KenjiBrown/rms-open-letter/-/merge_requests/new).

Institutional affiliation is provided for identification purposes only and does not constitute institutional endorsement.

Signed in support for Richard M Stallman,

- figosdev <faiflab@tutanota.com>
- Guillermo Molleda Jimena (Wed, 24 Mar 2021 11:59:24 +0100)
- Marcel Ventosa (Wed, 24 Mar 2021 05:48:58 -0600)
- Sandino Araico Sánchez <sandino@sandino.net>

